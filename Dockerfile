FROM alpine:latest
LABEL maintainer="Raphael Pala docker@pala.de"

# copy update script for docker secrets
COPY createsecret.sh /opt/createsecret.sh
COPY certbot /usr/sbin/certbot


# install docker and certbot with inwx plugin
RUN apk add --no-cache docker certbot && \
    apk add --no-cache --virtual .build-deps git && \
    git clone https://github.com/certbot/certbot /opt/certbot && \
    cd /opt/certbot/certbot-dns-cloudflare && \
    python setup.py install  && \
    rm -rf /opt/certbot && \
    apk del --no-cache .build-deps && \
    chmod +rwx /usr/sbin/certbot && \
    mkdir -p /etc/letsencrypt/

# create volume
VOLUME /etc/letsencrypt
    
# add cron job to check for renewal
RUN echo '0 */12 * * * /usr/sbin/certbot renew' | crontab -

# start cron in foreground
CMD [ "crond", "-f" ]
