
# certbot with cloudflare plugin and cron for renewal check
based on 
 - http://www.labouisse.com/how-to/2017/02/09/docker-secrets-with-lets-encrypt

Letsencrypt certbot with cloudflare plugin and cron, automatically checking for renewal and creating secrets from certificates in docker.

---

Docker must be in swarm mode
```sh
docker swarm init
```

---

## Start container or service
Start a container or service to make certbot check for renewal every 12 hours
```sh
  docker run --detach --restart=always --name certbot-container -v /var/run/docker.sock:/var/run/docker.sock raphpa/certbot-cloudflare
```

---
## Copy cloudflare configuration
Copy cloudflare.cfg with login credentials into container or mount it as secret to /etc/letsencrypt/cloudflare.cfg
```sh
  docker cp cloudflare.cfg certbot-container:/etc/letsencrypt/cloudflare.cfg
```


---
### cloudflare.cfg
The cloudflare configuration file should look as follows
```sh
dns_cloudflare_api_key = apikey
dns_cloudflare_email = email
```

---
## Create certificate
Create certificate with
```sh
  docker exec -it certbot-container certbot certonly --agree-tos --register-unsafely-without-email --server https://acme-v02.api.letsencrypt.org/directory --dns-cloudflare --dns-cloudflare-credentials /etc/letsencrypt/cloudflare.cfg --dns-cloudflare-propagation-seconds 60 -d <domain.tld> -d <*.domain.tld>
```

Or create a test certificate on the staging environment with
```sh
  docker exec -it certbot-container certbot certonly --agree-tos --register-unsafely-without-email --staging --dns-cloudflare --dns-cloudflare-credentials /etc/letsencrypt/cloudflare.cfg --dns-cloudflare-propagation-seconds 60  -d <domain.tld>
```

---
Secrets are automatically created from fullchain.pem and privkey.pem
Services using the renewed secrets are updated automatically if they have the label **'le_auto'** 

### docker-compose.yml
```sh
version: "3.6"
services:
  traefik:
    image: traefik:alpine
    deploy:
      replicas: 1
      restart_policy:
        condition: any
        delay: 5s
      placement:
        constraints:
          - node.role == manager
      labels:
        le_auto: "true"
...
```

---
## Shell access
The container shell can be accessed with
```sh
docker exec -it certbot-container /bin/sh
```